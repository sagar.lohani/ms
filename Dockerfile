FROM alpine:3.5
RUN apk add --update py2-pip
RUN pip install --upgrade pip
RUN mkdir /var/app
WORKDIR /var/app
COPY requirements.txt /var/app
COPY app.py /var/app/
RUN pip install --no-cache-dir -r /var/app/requirements.txt --trusted-host pypi.org --trusted-host files.pythonhosted.org
EXPOSE 5000
CMD ["python", "/var/app/app.py"]
